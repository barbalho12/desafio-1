# Desafio 1 - Fazer um aplicativo de lista de tarefas

## Descrição:

**Faça uma lista de tarefas que permita que o usuário adicione e remova itens. Seu aplicativo deve lembrar os itens toda vez que o usuário abrir o aplicativo.**

- [x] App desenvolvido em React no front: https://barbalho12.github.io/todolist-reactjs/
- [x] App desenvolvido em Java no back: https://staging-todolist.herokuapp.com/swagger-ui.html
- [x] Cadastro e Login básico de usuário 
> Email padrão criado para acessar: `teste@mail.com `

- [x] Criação de blocos de tarefas
- [x] Remoção de blocos de tarefas
- [x] Criação de tarefa
- [x] Finalização de tarefa
- [x] Opções de listagem de tarefas
- [x] Remoção de tarefa
- [x] Atualização assíncrona com websockets


## Requisitos de negócio

#### Como adicionar tarefas à lista
- [x] Seus usuários devem poder adicionar tarefas a lista ao digitar uma nova tarefa 

#### Como deletar itens da lista de tarefas
- [x] Seus usuários devem poder deletar coisas da lista de tarefas assim que completá-las.
> Completar a tarefa e remover são operações distintas na solução desenvolvida

#### Como programar o botão “Reset”
- [x] Explicação: Para facilitar o uso do seu aplicativo, adicionaremos um botão para limpar as entradas colocadas pelo usuário na lista. 
> Existe a opção de remover o bloco de tarefas inteiro

#### Como ter mais uma lista de tarefas - Plus
- [x] Seus usuários devem poder ter mais de uma lista de tarefas

## Pontos a serem avaliados
- Qualidade do Código (**Lint** do Eclipse e VSCode)
- Organização do código (**Organização das reponsabilidades em pacotes**, **Single-responsibility**)
- Paradigma de Programação (Orientação a Objetos / Funcional) (**OO** no back e **Funcional** no Front)
- Uso de estruturas de dados (**Objetos** e **Listas**)
- Uso de padrões de projeto (**Spring Framework** aplica vários dos padrões de projeto)
- Documentação do Código / API (**Java Doc**, **Swagger**)
- Teste de Software (**JUnit**)
- Automatizadores/Aceleradores de código (**Docker** para preparar ambiente, **pipeline**)
- API (**Rest**)
- Interface (**React JS**)

## Como participar do Desafio

1. Forkar este reposítório  (**OK**)
2. Realizar um clone em seu computador (**OK**)
3. Desenvolver a Solução (**OK**)
4. Ao final de cada dia uma entrega deve ser realizada em seu repositório (**OK**)
> Foram criadas issues descrevendo cada entrega
5. Submeter um pull request com a Solução, antes do Prazo Final. (**OK**)

## Plus

- Criação de uma plipeline(Gitlab, Jenkins, CircleCI) (**OK**)
> Pipeline no Gitlab
- Deploy da aplicação(Heroku, Openshift, AWS, Azure, Digital Ocean) Gitlab (**OK**)
> Deploy do back no [Heroku](https://staging-todolist.herokuapp.com/swagger-ui.html) e Front no [Github pages](https://barbalho12.github.io/todolist-reactjs)
- Automatização dos Testes(Selenium, Cypress, Protractor, Jest)




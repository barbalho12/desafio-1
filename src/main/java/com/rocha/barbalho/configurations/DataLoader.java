package com.rocha.barbalho.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.rocha.barbalho.models.Task;
import com.rocha.barbalho.models.TaskBlock;
import com.rocha.barbalho.models.User;
import com.rocha.barbalho.services.TaskBlockService;
import com.rocha.barbalho.services.TaskService;
import com.rocha.barbalho.services.UserService;

/**
 * Creation of dummy data during startup
 * 
 * @author Felipe Barbalho
 *
 */
@Component
public class DataLoader implements ApplicationRunner {

	@Value("${spring.jpa.hibernate.ddl-auto}")
	private String dbmode;

	@Autowired
	private TaskService taskService;

	@Autowired
	private TaskBlockService taskBlockService;

	@Autowired
	private UserService userService;

	@Override
	public void run(ApplicationArguments args) throws Exception {

		if (dbmode.equals("create")) {
			User user1 = userService.save(new User("Testador", "teste@mail.com", "123"));
			TaskBlock diadia = taskBlockService.save(new TaskBlock("Lembrar", user1));
			TaskBlock viagem = taskBlockService.save(new TaskBlock("Viagem", user1));
			taskService.save(new Task("Fazer exercício de matemática", diadia));
			taskService.save(new Task("Praticar natação", diadia));
			taskService.save(new Task("Comprar roupas", viagem));
		}
	}

}

package com.rocha.barbalho.controllers.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.rocha.barbalho.models.TaskBlock;
import com.rocha.barbalho.services.TaskBlockService;
import com.rocha.barbalho.statics.APIOperations;

import io.swagger.annotations.ApiOperation;

/**
 * Controller of BlockTask
 * 
 * @author Felipe Barbalho
 *
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/blocks")
public class TaskBlockController {

	@Autowired
	private TaskBlockService taskBlockService;

	@ApiOperation(value = APIOperations.OPERATION_BLOCKS_GET_ALL)
	@GetMapping
	private List<TaskBlock> findAll(@RequestHeader("user") String userEmail) {
		return taskBlockService.findAll(userEmail);
	}

	@ApiOperation(value = APIOperations.OPERATION_BLOCKS_FIND)
	@GetMapping("/{id}")
	public TaskBlock findById(@PathVariable Long id) {
		return taskBlockService.findById(id);
	}

	@ApiOperation(value = APIOperations.OPERATION_BLOCKS_SAVE)
	@PostMapping("/{taskBlockName}")
	@ResponseStatus(HttpStatus.CREATED)
	public TaskBlock save(@RequestHeader("user") String userEmail, @PathVariable String taskBlockName) {
		return taskBlockService.save(userEmail, taskBlockName);
	}

	@ApiOperation(value = APIOperations.OPERATION_BLOCKS_UPDATE)
	@PutMapping("/{id}")
	public TaskBlock update(@Valid @RequestBody TaskBlock entity, @PathVariable Long id) {
		return taskBlockService.update(id, entity);
	}

	@ApiOperation(value = APIOperations.OPERATION_BLOCKS_DELETE)
	@DeleteMapping("/{id}")
	public void delete(@PathVariable(value = "id") Long id) {
		taskBlockService.delete(id);
	}

}

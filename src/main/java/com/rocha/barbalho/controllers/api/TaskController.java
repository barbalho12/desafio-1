package com.rocha.barbalho.controllers.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.rocha.barbalho.models.Task;
import com.rocha.barbalho.services.TaskService;
import com.rocha.barbalho.statics.APIOperations;

import io.swagger.annotations.ApiOperation;

/**
 * Controller of Task
 * 
 * @author Felipe Barbalho
 *
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/tasks")
public class TaskController {

	@Autowired
	private TaskService taskService;

	@ApiOperation(value = APIOperations.OPERATION_TASKS_GET_ALL)
	@GetMapping
	private List<Task> findAll(@RequestHeader("block") long blockId) {
		return taskService.findAll(blockId);
	}

	@ApiOperation(value = APIOperations.OPERATION_TASKS_GET_ALL_END)
	@GetMapping("/completeds")
	private List<Task> findAllCompleted(@RequestHeader("block") long blockId) {
		return taskService.findAllCompleted(blockId);
	}

	@ApiOperation(value = APIOperations.OPERATION_TASKS_GET_ALL_LEFTS)
	@GetMapping("/lefts")
	private List<Task> findAllLeft(@RequestHeader("block") long blockId) {
		return taskService.findAllLeft(blockId);
	}

	@ApiOperation(value = APIOperations.OPERATION_TASKS_FIND)
	@GetMapping("/{id}")
	public Task findById(@PathVariable Long id) {
		return taskService.findById(id);
	}

	@ApiOperation(value = APIOperations.OPERATION_TASKS_SAVE)
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Task save(@Valid @RequestBody Task entity) {
		return taskService.save(entity);
	}

	@ApiOperation(value = APIOperations.OPERATION_TASKS_UPDATE)
	@PutMapping("/{id}")
	public Task update(@Valid @RequestBody Task entity, @PathVariable Long id) {
		return taskService.update(id, entity);
	}

	@ApiOperation(value = APIOperations.OPERATION_TASKS_SET_STATE)
	@PutMapping("/{id}/set-completed/{isCompleted}")
	public Task setCompleted(@PathVariable Long id, @PathVariable Boolean isCompleted) {
		return taskService.setCompleted(id, isCompleted);
	}

	@ApiOperation(value = APIOperations.OPERATION_TASKS_DELETE)
	@DeleteMapping("/{id}")
	public void delete(@PathVariable(value = "id") Long id) {
		taskService.delete(id);
	}

}

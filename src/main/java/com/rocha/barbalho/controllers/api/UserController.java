package com.rocha.barbalho.controllers.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.rocha.barbalho.models.User;
import com.rocha.barbalho.services.UserService;
import com.rocha.barbalho.statics.APIOperations;

import io.swagger.annotations.ApiOperation;

/**
 * Controller of User
 * 
 * @author Felipe Barbalho
 *
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/users")
public class UserController {

	@Autowired
	private UserService userService;

	@ApiOperation(value = APIOperations.OPERATION_USERS_GET_ALL)
	@GetMapping
	private List<User> findAll() {
		return userService.findAll();
	}

	@ApiOperation(value = APIOperations.OPERATION_USERS_FIND)
	@GetMapping("/{id}")
	public User findById(@PathVariable Long id) {
		return userService.findById(id);
	}

	@ApiOperation(value = APIOperations.OPERATION_USERS_FIND_BY_EMAIL)
	@GetMapping("/by-email/{email}")
	private User findByEmail(@PathVariable String email) {
		return userService.findByEmail(email);
	}

	@ApiOperation(value = APIOperations.OPERATION_USERS_SAVE)
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public User save(@Valid @RequestBody User entity) {
		return userService.save(entity);
	}

	@ApiOperation(value = APIOperations.OPERATION_USERS_UPDATE)
	@PutMapping("/{id}")
	public User update(@Valid @RequestBody User entity, @PathVariable Long id) {
		return userService.update(id, entity);
	}

	@ApiOperation(value = APIOperations.OPERATION_USERS_DELETE)
	@DeleteMapping("/{id}")
	public void delete(@PathVariable(value = "id") Long id) {
		userService.delete(id);
	}

}

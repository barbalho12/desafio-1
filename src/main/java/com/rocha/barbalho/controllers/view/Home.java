package com.rocha.barbalho.controllers.view;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.rocha.barbalho.models.Task;
import com.rocha.barbalho.services.TaskService;

/**
 * Thymeleaf controller access
 * 
 * @author Felipe Barbalho
 *
 */
@Controller
public class Home {

	@Autowired
	private TaskService taskService;

	@GetMapping("/")
	public String index(Model model) {
		model.addAttribute("tasks", taskService.findAll());
		model.addAttribute("task", new Task());
		return "index";
	}

	@PostMapping("/save")
	public String addUser(@Valid Task task, BindingResult result, Model model) {
		taskService.save(task);
		model.addAttribute("tasks", taskService.findAll());
		return "redirect:/";
	}
}

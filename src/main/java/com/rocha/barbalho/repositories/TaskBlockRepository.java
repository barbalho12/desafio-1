package com.rocha.barbalho.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rocha.barbalho.models.TaskBlock;
import com.rocha.barbalho.models.User;

/**
 * Integrates JPA with Hibernate provided by Spring to persist TaskBlock
 * 
 * @author Felipe Barbalho
 *
 */
@Repository
public interface TaskBlockRepository extends JpaRepository<TaskBlock, Long> {

	List<TaskBlock> findAllByUser(User user);

}

package com.rocha.barbalho.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rocha.barbalho.models.Task;
import com.rocha.barbalho.models.TaskBlock;

/**
 * Integrates JPA with Hibernate provided by Spring to persist Task
 * 
 * @author Felipe Barbalho
 *
 */
@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
	
	List<Task> findAllByOrderByIdAsc(); 

	List<Task> findByCompletedTrueOrderByIdAsc(); 

	List<Task> findByCompletedFalseOrderByIdAsc();

	List<Task> findAllByBlock(TaskBlock block);

	List<Task> findByCompletedTrueAndBlockOrderByIdAsc(TaskBlock block);

	List<Task> findByCompletedFalseAndBlockOrderByIdAsc(TaskBlock block);

}

package com.rocha.barbalho.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rocha.barbalho.models.User;

/**
 * Integrates JPA with Hibernate provided by Spring to persist User
 * 
 * @author Felipe Barbalho
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	User findByEmail(String email);
	
}

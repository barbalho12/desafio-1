package com.rocha.barbalho.services;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.rocha.barbalho.exceptions.BadRequestServiceExcepetion;
import com.rocha.barbalho.exceptions.NotFoundServiceException;
import com.rocha.barbalho.models.TaskBlock;
import com.rocha.barbalho.models.User;
import com.rocha.barbalho.repositories.TaskBlockRepository;
import com.rocha.barbalho.statics.StaticMessages;

/**
 * Apply business rules on controller access to repository TaskBlock data
 * 
 * @author Felipe Barbalho
 *
 */
@Service
public class TaskBlockService {

	@Autowired
	private SimpMessagingTemplate template;

	private static final String TOPIC_WS = "/topic/block";

	@Autowired
	private TaskBlockRepository taskBlockRepository;
	
	@Autowired
	private UserService userService; 

	public List<TaskBlock> findAll(String userEmail) {
		User user = userService.findByEmail(userEmail);
		return taskBlockRepository.findAllByUser(user);
	}

	public void delete(Long id) {
		if (taskBlockRepository.existsById(id)) {
			taskBlockRepository.deleteById(id);
			template.convertAndSend(TOPIC_WS, "delete");
		} else {
			throw new NotFoundServiceException(String.format(StaticMessages.VALIDATION_TASK_BLOCK_NOT_FOUND, id));
		}
	}

	public TaskBlock update(Long id, @Valid TaskBlock taskBlock) {
		if (taskBlock == null) {
			throw new BadRequestServiceExcepetion(StaticMessages.VALIDATION_TASK_BLOCK_NOT_NULL);
		}
		if (taskBlockRepository.existsById(id)) {
			TaskBlock taskBlockUpdate = taskBlockRepository.save(taskBlock);
			template.convertAndSend(TOPIC_WS, "update");
			return taskBlockUpdate;
		} else {
			throw new NotFoundServiceException(String.format(StaticMessages.VALIDATION_TASK_BLOCK_NOT_FOUND, id));
		}

	}

	public TaskBlock findById(Long id) {
		Optional<TaskBlock> taskBlock = taskBlockRepository.findById(id);
		if (taskBlock.isPresent()) {
			return taskBlock.get();
		} else {
			throw new NotFoundServiceException(String.format(StaticMessages.VALIDATION_TASK_BLOCK_NOT_FOUND, id));
		}
	}

	public TaskBlock save(@Valid TaskBlock task) {
		if (task == null) {
			throw new BadRequestServiceExcepetion(StaticMessages.VALIDATION_TASK_BLOCK_NOT_NULL);
		}
		task.setId(null);
		TaskBlock taskSaved = taskBlockRepository.save(task);
		template.convertAndSend(TOPIC_WS, "save");
		return taskSaved;
	}

	public TaskBlock save(String userEmail, String taskBlockName) {
		User user = userService.findByEmail(userEmail);
		TaskBlock taskBlock = new TaskBlock(taskBlockName, user);
		return save(taskBlock);
	}

}

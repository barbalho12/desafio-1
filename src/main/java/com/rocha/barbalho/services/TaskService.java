package com.rocha.barbalho.services;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.rocha.barbalho.exceptions.BadRequestServiceExcepetion;
import com.rocha.barbalho.exceptions.NotFoundServiceException;
import com.rocha.barbalho.models.Task;
import com.rocha.barbalho.models.TaskBlock;
import com.rocha.barbalho.repositories.TaskRepository;
import com.rocha.barbalho.services.interfaces.ITaskService;
import com.rocha.barbalho.statics.StaticMessages;

/**
 * Apply business rules on controller access to repository Task data
 * 
 * @author Felipe Barbalho
 *
 */
@Service
public class TaskService implements ITaskService{

	@Autowired
	private SimpMessagingTemplate template;
	
	private static final String TOPIC_WS_TASK = "/topic/tasks";

	@Autowired
	private TaskRepository taskReposiotory;
	
	@Autowired
	private TaskBlockService taskBlockService;

	@Override
	public List<Task> findAll() {
		return taskReposiotory.findAllByOrderByIdAsc();
	}

	@Override
	public List<Task> findAllCompleted() {
		return taskReposiotory.findByCompletedTrueOrderByIdAsc();
	}

	@Override
	public List<Task> findAllLeft() {
		return taskReposiotory.findByCompletedFalseOrderByIdAsc();
	}

	@Override
	public void delete(Long id) {
		if (taskReposiotory.existsById(id)) {
			taskReposiotory.deleteById(id);
			template.convertAndSend(TOPIC_WS_TASK, "delete");
		} else {
			throw new NotFoundServiceException(String.format(StaticMessages.VALIDATION_TASK_NOT_FOUND, id));
		}
	}

	@Override
	public Task update(Long id, @Valid Task task) {
		if (task == null) {
			throw new BadRequestServiceExcepetion(StaticMessages.VALIDATION_TASK_NOT_NULL);
		}
		if (taskReposiotory.existsById(id)) {
			Task taskUpdate = taskReposiotory.save(task);
			template.convertAndSend(TOPIC_WS_TASK, "update");
			return taskUpdate;
		} else {
			throw new NotFoundServiceException(String.format(StaticMessages.VALIDATION_TASK_NOT_FOUND, id));
		}

	}

	@Override
	public Task setCompleted(Long id, Boolean isCompleted) {
		Optional<Task> task = taskReposiotory.findById(id);
		if (task.isPresent()) {
			task.get().setCompleted(isCompleted);
			Task taskCompleted = taskReposiotory.save(task.get());
			template.convertAndSend(TOPIC_WS_TASK, "setCompleted");
			return taskCompleted;
		} else {
			throw new NotFoundServiceException(String.format(StaticMessages.VALIDATION_TASK_NOT_FOUND, id));
		}
	}

	@Override
	public Task findById(Long id) {
		Optional<Task> task = taskReposiotory.findById(id);
		if (task.isPresent()) {
			return task.get();
		} else {
			throw new NotFoundServiceException(String.format(StaticMessages.VALIDATION_TASK_NOT_FOUND, id));
		}
	}

	@Override
	public Task save(@Valid Task task) {
		if (task == null) {
			throw new BadRequestServiceExcepetion(StaticMessages.VALIDATION_TASK_NOT_NULL);
		}
		task.setId(null);
		Task taskCompleted = taskReposiotory.save(task);
		template.convertAndSend(TOPIC_WS_TASK, "save");
		return taskCompleted;
	}
	

	public Task save(@Valid Task task, long blockId) {
		TaskBlock taskBlock = taskBlockService.findById(blockId);
		task.setBlock(taskBlock);
		return save(task);
	}

	public List<Task> findAll(long blockId) {
		TaskBlock taskBlock = taskBlockService.findById(blockId);
		return taskReposiotory.findAllByBlock(taskBlock);
	}
	

	public List<Task> findAllCompleted(long blockId) {
		TaskBlock taskBlock = taskBlockService.findById(blockId);
		return taskReposiotory.findByCompletedTrueAndBlockOrderByIdAsc(taskBlock);
	}


	public List<Task> findAllLeft(long blockId) {
		TaskBlock taskBlock = taskBlockService.findById(blockId);
		return taskReposiotory.findByCompletedFalseAndBlockOrderByIdAsc(taskBlock);
	}

}

package com.rocha.barbalho.services;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.rocha.barbalho.exceptions.BadRequestServiceExcepetion;
import com.rocha.barbalho.exceptions.NotFoundServiceException;
import com.rocha.barbalho.models.User;
import com.rocha.barbalho.repositories.UserRepository;
import com.rocha.barbalho.statics.StaticMessages;

/**
 * Apply business rules on controller access to repository User data
 * 
 * @author Felipe Barbalho
 *
 */
@Service
public class UserService {

	@Autowired
	private SimpMessagingTemplate template;

	private static final String TOPIC_WS = "/topic/user";

	@Autowired
	private UserRepository userRepository;

	public List<User> findAll() {
		return userRepository.findAll();
	}

	public void delete(Long id) {
		if (userRepository.existsById(id)) {
			userRepository.deleteById(id);
			template.convertAndSend(TOPIC_WS, "delete");
		} else {
			throw new NotFoundServiceException(String.format(StaticMessages.VALIDATION_USER_NOT_FOUND, id));
		}
	}

	public User update(Long id, @Valid User user) {
		if (user == null) {
			throw new BadRequestServiceExcepetion(StaticMessages.VALIDATION_USER_NOT_NULL);
		}
		if (userRepository.existsById(id)) {
			User userUpdate = userRepository.save(user);
			template.convertAndSend(TOPIC_WS, "update");
			return userUpdate;
		} else {
			throw new NotFoundServiceException(String.format(StaticMessages.VALIDATION_USER_NOT_FOUND, id));
		}

	}

	public User findById(Long id) {
		Optional<User> user = userRepository.findById(id);
		if (user.isPresent()) {
			return user.get();
		} else {
			throw new NotFoundServiceException(String.format(StaticMessages.VALIDATION_USER_NOT_FOUND, id));
		}
	}

	public User save(@Valid User user) {
		if (user == null) {
			throw new BadRequestServiceExcepetion(StaticMessages.VALIDATION_USER_NOT_NULL);
		}
		user.setId(null);
		User userSaved = userRepository.save(user);
		template.convertAndSend(TOPIC_WS, "save");
		return userSaved;
	}

	public User findByEmail(@NotEmpty String email) {
		User user = userRepository.findByEmail(email);
		if (user == null) {
			throw new NotFoundServiceException(String.format(StaticMessages.VALIDATION_USER_EMAIL_NOT_FOUND, email));
		}
		return userRepository.findByEmail(email);
	}

}

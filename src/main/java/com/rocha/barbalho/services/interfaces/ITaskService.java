package com.rocha.barbalho.services.interfaces;

import java.util.List;

import javax.validation.Valid;

import com.rocha.barbalho.models.Task;

public interface ITaskService {

	/**
	 * Query list of all tasks
	 * @return Lists of all tasks
	 */
	public List<Task> findAll();

	/**
	 * Query list of all completed tasks
	 * @return List of all completed tasks
	 */
	public List<Task> findAllCompleted();

	/**
	 * Query list of all incomplete tasks
	 * @return List of all incomplete tasks
	 */
	public List<Task> findAllLeft();

	/**
	 * Remove task by id
	 * @param id Task id
	 */
	public void delete(Long id);

	/**
	 * Update task by id
	 * @param id Task id
	 * @param task Task with updated attributes
	 * @return Updated task 
	 */
	public Task update(Long id, @Valid Task task);

	/**
	 * 
	 * @param id Task id
	 * @param isCompleted Boolean value with true or false state for the complete state of the task
	 * @return Updated task
	 */
	public Task setCompleted(Long id, Boolean isCompleted);

	/**
	 * 
	 * @param id Task id
	 * @return Consulted task 
	 */
	public Task findById(Long id);

	/**
	 * Register a new task
	 * @param task New task
	 * @return Registered task
	 */
	public Task save(@Valid Task task);

}

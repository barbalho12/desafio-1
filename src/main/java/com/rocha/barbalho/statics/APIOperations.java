package com.rocha.barbalho.statics;

/**
 * Static description of existing REST operations
 * 
 * @author Felipe Barbalho
 *
 */
public class APIOperations {
	
	public static final String OPERATION_BLOCKS_GET_ALL = "Retorna todas os blocos";
	public static final String OPERATION_BLOCKS_SAVE = "Cria um bloco";
	public static final String OPERATION_BLOCKS_FIND = "Consulta um bloco pelo id";
	public static final String OPERATION_BLOCKS_UPDATE = "Atualiza um bloco pelo id";
	public static final String OPERATION_BLOCKS_DELETE = "Remove um bloco pelo id";

	public static final String OPERATION_TASKS_GET_ALL = "Retorna todas as tarefas";
	public static final String OPERATION_TASKS_GET_ALL_END = "Retorna todas as tarefas finalizadas";
	public static final String OPERATION_TASKS_GET_ALL_LEFTS = "Retorna todas as tarefas não finalizadas";
	public static final String OPERATION_TASKS_SAVE = "Cria uma tarefa";
	public static final String OPERATION_TASKS_FIND = "Consulta uma tarefa pelo id";
	public static final String OPERATION_TASKS_UPDATE = "Atualiza uma tarefa pelo id";
	public static final String OPERATION_TASKS_SET_STATE = "Altera o estado para completo ou incompleto de uma tarefa";
	public static final String OPERATION_TASKS_DELETE = "Remove uma tarefa pelo id";
	
	
	
	public static final String OPERATION_USERS_GET_ALL = "Retorna todas os usuários";
	public static final String OPERATION_USERS_SAVE = "Cria um usuário";
	public static final String OPERATION_USERS_FIND = "Consulta um usuário pelo id";
	public static final String OPERATION_USERS_FIND_BY_EMAIL = "Consulta um usuário pelo email";
	public static final String OPERATION_USERS_UPDATE = "Atualiza um usuário pelo id";
	public static final String OPERATION_USERS_DELETE = "Remove um usuário pelo id";

}

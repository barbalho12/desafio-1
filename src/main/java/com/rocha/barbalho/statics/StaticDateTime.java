package com.rocha.barbalho.statics;

/**
 * Sets date format for global use
 * 
 * @author Felipe Barbalho
 *
 */
public class StaticDateTime {
	
	public static final String DATETIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

}

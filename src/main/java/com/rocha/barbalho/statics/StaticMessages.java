package com.rocha.barbalho.statics;

/**
 * Defines standard return messages at the service layer
 * 
 * @author Felipe Barbalho
 *
 */
public class StaticMessages {

	public static final String VALIDATION_TASK_DESCRIPTION = "Descrição de tarefa não pode ser vazia";
	public static final String VALIDATION_TASK_NOT_FOUND = "A tarefa com id %s não foi encontrada";
	public static final String VALIDATION_TASK_NOT_NULL = "A tarefa com id %s não foi encontrada";
	public static final String VALIDATION_USER_NAME = "O nome do usuário não pode ser nulo";
	public static final String VALIDATION_USER_EMAIL = "O email do usuário não pode ser nulo";
	public static final String VALIDATION_USER_PASSWORD = "O password do usuário não pode ser nulo";
	public static final String VALIDATION_TASK_BLOCK_NAME = "O nome do bloco não pode ser nulo";
	public static final String VALIDATION_TASK_BLOCK_NOT_FOUND = "O bloco com id %s não foi encontrado";
	public static final String VALIDATION_TASK_BLOCK_NOT_NULL = "O bloco com id %s não foi encontrado";
	public static final String VALIDATION_USER_NOT_FOUND = "O usuário com id %s não foi encontrado";
	public static final String VALIDATION_USER_EMAIL_NOT_FOUND = "O usuário com email %s não foi encontrado";
	public static final String VALIDATION_USER_NOT_NULL = "O usuário com id %s não foi encontrado";

}

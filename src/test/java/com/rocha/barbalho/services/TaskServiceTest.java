package com.rocha.barbalho.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.context.SpringBootTest;

import com.rocha.barbalho.exceptions.BadRequestServiceExcepetion;
import com.rocha.barbalho.exceptions.NotFoundServiceException;
import com.rocha.barbalho.models.Task;
import com.rocha.barbalho.models.TaskBlock;
import com.rocha.barbalho.models.User;

@SpringBootTest
public class TaskServiceTest {

	@Autowired
	private TaskService taskService;

	@Autowired
	private TaskBlockService taskBlockService;

	@Autowired
	private UserService userService;

	public static List<Task> taskList;

	@BeforeEach
	void init() {
		User user1 = userService.save(new User("Testador", "teste@mail.com", "123"));
		TaskBlock diadia = taskBlockService.save(new TaskBlock("Lembrar", user1));
		TaskBlock viagem = taskBlockService.save(new TaskBlock("Viagem", user1));
		Task task1 = taskService.save(new Task("Fazer teste 1", diadia));
		Task task2 = taskService.save(new Task("Fazer teste 2", diadia));
		Task task3 = taskService.save(new Task("Fazer teste com descrição bem grande com muitas palavras ", viagem));
		taskList = Arrays.asList(task1, task2, task3);
	}

	@AfterEach
	public void done() {
		List<Task> tasks = taskService.findAll();
		tasks.forEach(t -> taskService.delete(t.getId()));
	}

	@DisplayName("Lista todas as tarefas")
	@Test
	public void findAllTest() {
		done();
		List<Task> tasks = taskService.findAll();
		assertEquals(0, tasks.size(), "Zero elementos de retorno");
		taskList.forEach(t -> taskService.save(t));
		tasks = taskService.findAll();
		assertEquals(taskList.size(), tasks.size(), "Retorno vazio");
	}

	@DisplayName("Lista todas as tarefas finalizadas")
	@Test
	public void findAllCompletedTest() {
		taskList.get(0).setCompleted(true);
		taskList.get(2).setCompleted(true);
		taskList.forEach(t -> taskService.save(t));

		List<Task> tasks = taskService.findAllCompleted();
		assertEquals(2, tasks.size());
		tasks.forEach(t -> assertEquals(true, t.isCompleted()));

	}

	@DisplayName("Lista todas as tarefas não finalizadas")
	@Test
	public void findAllLeftTest() {
		done();
		taskList.get(0).setCompleted(true);
		taskList.forEach(t -> taskService.save(t));

		List<Task> tasks = taskService.findAllLeft();
		assertEquals(2, tasks.size());
		tasks.forEach(t -> assertEquals(false, t.isCompleted()));
	}

	@DisplayName("Remove tarefa")
	@Test
	public void deleteTest() {
		taskList.forEach(t -> taskService.save(t));
		List<Task> tasks = taskService.findAll();

		long idTaskRemoved = tasks.get(0).getId();

		taskService.delete(idTaskRemoved);

		assertThrows(NotFoundServiceException.class, () -> {
			taskService.findById(idTaskRemoved);
		});

		tasks = taskService.findAll();
		tasks.forEach(t -> assertNotEquals(idTaskRemoved, t.getId()));
	}

	@DisplayName("Atualiza tarefa")
	@Test
	public void updateTest() {
		Task task = taskList.get(0);
		Task taskEditada = taskService.update(task.getId(), new Task("Nova descrição", task.getBlock()));
		assertEquals("Nova descrição", taskEditada.getDescription());
		assertNotEquals(task.getDescription(), taskEditada.getDescription());
	}

	@DisplayName("Busca tarefa pelo Id")
	@Test
	public void findById() {
		Task task = taskList.get(0);
		Task task2 = taskService.findById(task.getId());
		assertEquals(task, task2);
	}

	@DisplayName("Cadastra tarefa")
	@Test
	public void saveTest() {
		Task task = taskService.save(new Task("Fazer teste 1", taskList.get(0).getBlock()));
		Long taskId = task.getId();
		Task task2 = taskService.findById(taskId);
		assertEquals(task, task2);
	}

	@DisplayName("Erro ao Cadastrar tarefa sem descrição")
	@Test
	public void saveTestDescriptionNull() {
		Task task = new Task();
		assertThrows(ConstraintViolationException.class, () -> {
			taskService.save(task);
		});
	}

	@DisplayName("Erro ao Cadastrar tarefa nula")
	@Test
	public void saveTestNull() {
		assertThrows(BadRequestServiceExcepetion.class, () -> {
			taskService.save(null);
		});
	}

	@DisplayName("Erro ao Deletar tarefa com id inexistente")
	@Test
	public void deleteNotFound() {
		assertThrows(NotFoundServiceException.class, () -> {
			taskService.delete(999999l);
		});
	}

	@DisplayName("Erro ao Atualizar tarefa com id inexistente")
	@Test
	public void updateNotFound() {
		assertThrows(NotFoundServiceException.class, () -> {
			taskService.update(999999l, new Task("update", taskList.get(0).getBlock()));
		});
	}

	@DisplayName("Erro ao Atualizar descrição tarefa com valor nulo")
	@Test
	public void updateDescriptionNull() {
		Task taskSaved = taskService.save(getTaskList().get(0));
		taskSaved.setDescription(null);
		assertThrows(ConstraintViolationException.class, () -> {
			taskService.update(taskSaved.getId(), new Task(null, taskSaved.getBlock()));
		});
	}

	public static List<Task> getTaskList() {
		return taskList;
	}

	public static void setTaskList(List<Task> taskList) {
		TaskServiceTest.taskList = taskList;
	}

}
